# What is the ELK stack?
__The ELK stack is an acronym used to describe a stack that comprises of three popular open-source projects: Elasticsearch, Logstash, and Kibana. Often referred to as Elasticsearch, the ELK stack gives you the ability to aggregate logs from all your systems and applications, analyze these logs, and create visualizations for application and infrastructure monitoring, faster troubleshooting, security analytics, and more.__

# What is ES
__[Elasticsearch](https://en.wikipedia.org/wiki/Elasticsearch) is an open-source, [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer)(Representational state transfer), distributed search and analytics engine built on Apache Lucene. Elasticsearch has quickly become the most popular search engine, and is commonly used for log analytics, full-text search, security intelligence, business analytics, and operational intelligence use cases. Elasticsearch is developed in Java.__

# ES benefits
* **Fast time-to-value**
  * Elasticsearch offers simple REST based APIs, a simple HTTP interface, and uses       schema-free JSON documents, making it easy to get started and quickly build applications for a variety of use-cases.
* **Near real-time operations**
  * Elasticsearch operations such as reading or writing data usually take less than a second to complete.
* **High performance**
  * The distributed nature of Elasticsearch enables it to process large volumes of data in parallel, quickly finding the best matches for your queries.
* **Complimentary tooling and plugins**
  * Elasticsearch comes integrated with Kibana, a popular visualization and reporting tool. It also offers integration with Beats and Logstash, while enable you to easily transform source data and load it into your Elasticsearch cluster.
* **Easy application development**
  * Elasticsearch provides support for various languages including Java, Python, PHP, JavaScript, Node.js, Ruby, and many more.

# What is Logstash
__Logstash is a light-weight, open-source, server-side data processing pipeline that allows you to collect data from a variety of sources, transform it on the fly, and send it to your desired destination. It is most often used as a data pipeline for Elasticsearch, an open-source analytics and search engine. Because of its tight integration with Elasticsearch, powerful log processing capabilities, and over 200 pre-built open-source plugins that can help you easily index your data, Logstash is a popular choice for loading data into Elasticsearch.__


```diff
- Data ingestion
+ Data ingestion is a process by which data is moved from one or more sources to a destination where it can be stored and further analyzed. The data might be in different formats and come from various sources, including RDBMS, other types of databases, S3 buckets, CSVs, or from streams
```

# Logstash benefits
* __Easily load unstructured data__
  * Logstash allows you to easily ingest unstructured data from a variety of data sources including system logs, website logs, and application server logs. 
* __Pre-built filters__
  * Logstash offers pre-built filters, so you can readily transform common data types, index them in Elasticsearch, and start querying without having to build custom data transformation pipelines.
* __Flexible plugin architecture__
  * over 200 plugins already available on Github.

# What is Kibana
__Kibana is an open-source data visualization and exploration tool used for log and time-series analytics, application monitoring, and operational intelligence use cases. It offers powerful and easy-to-use features such as histograms, line graphs, pie charts, heat maps, and built-in geospatial support. Also, it provides tight integration with Elasticsearch, a popular analytics and search engine, which makes Kibana the default choice for visualizing data stored in Elasticsearch.__

# Kibana benefits
* __Interactive charts__
  * Kibana offers intuitive charts and reports that you can use to interactively navigate through large amounts of log data.
* __Mapping support__
  * Kibana comes with powerful geospatial capabilities so you can seamlessly layer in geographical information on top of your data and visualize results on maps.
* __Pre-built aggregations and filters__
* we can run a variety of analytics like histograms, top-N queries, and trends with just a few clicks.

# ES basic concept
* Cluster
  * A Cluster is a collection of nodes(servers)
  * Consists of one or more nodes, depending on the scale
    * can contain as many nodes as you want
  * Together, these nodes contain all data
  * A cluster provides indexing and search capability across all nodes
  * Identified by a unique name (defaults to __elasticsearch__ )

* Node
  * A single server that is part of a cluster
  * Store searchable data
    * Store all data if there is only one node in the cluster, or part of the data if there are multiple nodes
  * Participates in a cluster's indexing and search capabilities
  * Identified by a name
  * A node joins a cluster named by __elasticsearch__ by default
  * Starting a single node on a network will by default create single-node cluster named __elasticsearch__

* Shards
  * An index can be divided into multiple pieces called shards
    * Useful if an index contains more data than the hardware of a node can store(e.g. 1TB data on a 500 GB disk)
  * A shard is a fully functional and independent index
    * Can be stored on any node in a cluster
  * The number of shard can be specified when creating an index
  * Allowed to distribute and parallelize operations across shards, which increases performance

* Replicas
  * A replica is a copy of shard
  * Provides high availability in case a shard or node fails
    * A replica never resides on the same node as the original shard
  * Allows scaling search volume, because search queries can be execute on all replicas in parallel

* Index
  * A collection of documents
  * Identified by a name
  * Used when indexing, searching, updating and deleting document withing the index

* Documents
  * A basic unit of information that can be indexed
  * Consists of fields, which are key/value pairs
    * A value can be a string, date, object, etc.
  * Corresponds to an object in an object-oriented programming language a document can be a single user, order, product, etc.
  * Document are expressed in JSON
  * We can store as many documnents within an index as we want
* Type
  * Represents a class/category of similar documnents
  * Consists of a name and a mapping
  * A index can have one ore more types defined, each with there own mapping
  * Stored within a metadata field named _type because __Lucence__ has no concept of document types
  * Searching for specific document types applies a filter on this field

* Mapping
  * Describes the fields that a document of a given type may have
  * Includes the data types for each field e.g. string, date, object, etc
  * Also includes information on how fields should be indexed and store by __Lucence__

# [Installing Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/targz.html)

* Step 1
  * $sudo apt install default-jdk
  * &sudo apt install elasticsearch

* Step 2
  * pip install elasticsearch

# Test

http://localhost:9200/

# Create and delete an index

* from elasticsearch import Elasticsearch
* es = Elasticsearch('http://localhost:9200')
* ## To create
  * es.indices.create(index='first_index', ignore=400)
* ## Exist or not
  * es.indices.exists(index="first_index")
* ## Delete
  * es.indices.delete(index='first_index')

# Insert and Get query

* from elasticsearch import Elasticsearch
* es = Elasticsearch('http://localhost:9200')
* doc1 = {"city":"New Delhi","country":"india"}
* doc2 = {"city":"London","country":"England"}
* doc3 = {"city":"Los Angeles","country":"USA"}
* ## Insert
  * es.index(index="cities", doc_type="places", id=1, body=doc1)
  * es.index(index="cities", doc_type="places", id=2, body=doc2)
  * es.index(index="cities", doc_type="places", id=3, body=doc3)
* ## Get 
  * result = es.get(index="cities", doc_type="places", id=2)

## Different search query for matching documents
* from elasticsearch import Elasticsearch
* es = Elasticsearch('http://localhost:9200')
* doc1 = {"sentence":"Today is a sunny day."}
* doc2 = {"sentence": "Today is a bright-sunny day"}
* es.index(index="english", doc_type="sentence", id=1, body=doc1)
* es.index(index="english", doc_type="sentence", id=2, body=doc2)
* ## Search 
  * result = es.search(index="english", body={"from":0,"size":0,"query":{"match":{"sentence":"SUNNY"}}})
    * ## total match documents
    * result = es.search(index="english", body={"from":0,"size":2,"query":{"match":{"sentence":"SUNNY"}}})
  * ## match_phrase
  * result = es.search(index="english", body={"from":0,"size":0,"query":{"match_phrase":{"sentence":"bright SUNNY"}}})
  * ## term - case sensitive
  * result = es.search(index="english", body={"from":0,"size":0,"query":{"term":{"sentence":"SUNNY"}}})
  * ## regexp
  * result = es.search(index="english", body={"from":0,"size":0,"query":{"regexp":{"sentence":".*"}}})
  * result = es.search(index="english", body={"from":0,"size":0,"query":{"regexp":{"sentence":"sun.*"}}})


# References

* [ Shay banon Blog](http://www.elasticsearch.org/blog/2010/02/08/youknowforsearch.html)
* [ Compass project](https://web.archive.org/web/20150317065626/http://www.compass-project.org/)